;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  var toggleForm = function() {
    $("#display_page_form").toggleClass("hide");
    $("#pageForm").toggleClass("hide");
  };
  if ($("#pageFormError").length > 0){
    toggleForm();
  };

  $(document).on('click', '#display_page_form', function(event) {
    console.log("event : ", event);
    console.log("this :", this);
    toggleForm();
  });
})(jQuery, this);
